// A Hello World! program in C#.

using System;

namespace HelloWorld.Model
{
    public class Person 
    {
		public string Name { get; set;}
        public Person(string name){
			Name = name;
		}
		
		public string SayHello(){
			return "Hello, " + Name;
		}
    }
}