// A Hello World! program in C#.
using System;
using HelloWorld.Model;

namespace HelloWorld
{
    class Hello 
    {
        static void Main() 
        {
			var name = Console.ReadLine();
			var p = new Person(name);
		
            Console.WriteLine(p.SayHello());

            // Keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}