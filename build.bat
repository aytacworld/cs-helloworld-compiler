@echo off
if not exist ".\bin" mkdir ".\bin"
echo # Building
C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe /t:exe /out:.\bin\HelloWorld.exe /recurse:.\src\*.cs
echo # Building success
echo # Running Application
.\bin\HelloWorld.exe